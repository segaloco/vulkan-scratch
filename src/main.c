#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>

#include "graphics/g_core.h"
#include "input/i_core.h"

#define	XRES	640
#define	YRES	480

/* Vulkan context */

#include <vulkan/vulkan.h>

extern int graphicsDeviceQueueIndex;
extern int surfaceDeviceQueueIndex;
extern VkDevice device;
extern VkExtent2D surfaceImageExtent;
extern VkSwapchainKHR swapchain;
extern uint32_t swapchainImageCount;
extern VkViewport viewport;
extern VkRenderPass renderPass;
extern VkPipeline graphicsPipeline;
extern VkFramebuffer *swapchainFramebuffers;
extern VkCommandBuffer *commandBuffers;
extern VkSemaphore imageAvailableSemaphore;
extern VkSemaphore renderFinishedSemaphore;

/* Vulkan context end */

int main(int argc, char *argv[])
{
	bool m_quiet;
	int framebufferIndex = 0;
	int c;
	bool running = true;
	const char *optstring = "q";
	VkQueue graphicsQueue;
	VkQueue presentQueue;

	while ((c = getopt(argc, argv, optstring)) != -1) {
		switch (c) {
		case 'q':
			m_quiet = true;
			break;
		case '?':
			fprintf(stderr, "usage: %s [-%s]\n", argv[0], optstring);
			break;
		}
	}

	g_init("Vulkan", !m_quiet, XRES, YRES);

	vkGetDeviceQueue(device, graphicsDeviceQueueIndex, 0, &graphicsQueue);
	vkGetDeviceQueue(device, surfaceDeviceQueueIndex, 0, &presentQueue);

	while (running) {
		VkCommandBufferBeginInfo commandBufferBeginInfo = { 0 };
		VkRenderPassBeginInfo renderPassBeginInfo = { 0 };
		VkClearValue clearColor = {{{ 0.0f, 0.0f, 0.0f, 1.0f }}};
		uint32_t imageIndex;
		VkPipelineStageFlags pipelineStageFlags = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		VkSubmitInfo submitInfo = { 0 };
		VkPresentInfoKHR presentInfo = { 0 };
		running = i_poll();

		commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		
		vkBeginCommandBuffer(commandBuffers[framebufferIndex], &commandBufferBeginInfo);
		vkCmdSetViewport(commandBuffers[framebufferIndex], 0, 1, &viewport);

		renderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
		renderPassBeginInfo.renderPass = renderPass;
		renderPassBeginInfo.framebuffer = swapchainFramebuffers[framebufferIndex];
		renderPassBeginInfo.renderArea.offset.x = 0;
		renderPassBeginInfo.renderArea.offset.y = 0;
		renderPassBeginInfo.renderArea.extent = surfaceImageExtent;
		renderPassBeginInfo.clearValueCount = 1;
		renderPassBeginInfo.pClearValues = &clearColor;

		vkCmdBeginRenderPass(commandBuffers[framebufferIndex], &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

		vkCmdBindPipeline(commandBuffers[framebufferIndex], VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline);

		vkCmdDraw(commandBuffers[framebufferIndex], 3, 1, 0, 0);

		vkCmdEndRenderPass(commandBuffers[framebufferIndex]);
		vkEndCommandBuffer(commandBuffers[framebufferIndex]);

		vkAcquireNextImageKHR(device, swapchain, UINT64_MAX, imageAvailableSemaphore, VK_NULL_HANDLE, &imageIndex);

		submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		submitInfo.waitSemaphoreCount = 1;
		submitInfo.pWaitSemaphores = &imageAvailableSemaphore;
		submitInfo.pWaitDstStageMask = &pipelineStageFlags;
		submitInfo.commandBufferCount = 1;
		submitInfo.pCommandBuffers = &commandBuffers[imageIndex];
		submitInfo.signalSemaphoreCount = 1;
		submitInfo.pSignalSemaphores = &renderFinishedSemaphore;

		vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE);

		presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
		presentInfo.waitSemaphoreCount = 1;
		presentInfo.pWaitSemaphores = &renderFinishedSemaphore;
		presentInfo.swapchainCount = 1;
		presentInfo.pSwapchains = &swapchain;
		presentInfo.pImageIndices = &imageIndex;

		vkQueuePresentKHR(presentQueue, &presentInfo);

		vkQueueWaitIdle(presentQueue);

		framebufferIndex++;

		if (framebufferIndex >= swapchainImageCount)
			framebufferIndex = 0;
	}

	g_free();

	return EXIT_SUCCESS;
}
