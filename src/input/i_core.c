#include <stdbool.h>

#include <X11/Xlib.h>
#include <X11/keysym.h>

extern Display *dpy;

#include "i_core.h"

bool i_poll(void)
{
    bool running = true;

    XEvent event;

    while (XPending(dpy) != 0) {
        XNextEvent(dpy, &event);

        switch (event.type) {
        case KeyPress:
            switch (XLookupKeysym(&event.xkey, 0)) {
            case XK_Escape:
                running = false;
            default:
                break;
            }
        default:
            break;
        }
    }

    return running;
}
