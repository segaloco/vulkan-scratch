#ifndef I_CORE_H
#define I_CORE_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

extern bool i_poll(void);

#ifdef __cplusplus
};
#endif /* __cplusplus */

#endif /* I_CORE_H */
