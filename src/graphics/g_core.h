#ifndef G_CORE_H
#define G_CORE_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <stdbool.h>

extern void g_init(const char *name, bool validation, unsigned int width, unsigned int height);
extern void g_free(void);

#ifdef __cplusplus
};
#endif /* __cplusplus */

#endif /* G_CORE_H */
