#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <vulkan/vulkan.h>

int graphicsDeviceQueueIndex = -1;
int surfaceDeviceQueueIndex = -1;
VkDevice device;
VkExtent2D surfaceImageExtent;
VkSwapchainKHR swapchain;
uint32_t swapchainImageCount;
VkViewport viewport;
VkRenderPass renderPass;
VkPipeline graphicsPipeline;
VkFramebuffer *swapchainFramebuffers;
VkCommandBuffer *commandBuffers;
VkSemaphore imageAvailableSemaphore;
VkSemaphore renderFinishedSemaphore;

static VkInstance instance;
static VkSurfaceKHR surface;
static VkPhysicalDevice physicalDevice;
static VkImageView *swapchainImageViews;
static VkShaderModule vertexShaderModule;
static VkShaderModule fragmentShaderModule;
static VkPipelineLayout pipelineLayout;
static VkCommandPool commandPool;

static void vk_init(const char *name, bool validation, unsigned int platformExtensionCount, const char *platformExtensions[]);
static void vk_open(unsigned int width, unsigned int height);
static void vk_close(void);
static void vk_free(void);

#include "g_core.h"

#include <X11/Xlib.h>
#include <vulkan/vulkan_xlib.h>

Display *dpy;
static Window window;

/*
 * g_init - initialize the graphics subsystem, X11 variant
 */
void g_init(const char *name, bool validation, unsigned int width, unsigned int height)
{
	const char *platformExtension = "VK_KHR_xlib_surface";

	XSetWindowAttributes windowAttributes = { 0 };

	VkXlibSurfaceCreateInfoKHR surfaceCreateInfo = { 0 };

	vk_init(name, validation, 1, &platformExtension);

	dpy = XOpenDisplay(NULL);
	windowAttributes.event_mask = KeyPressMask | KeyReleaseMask;
	window = XCreateWindow(
		dpy,
		XDefaultRootWindow(dpy),
		100,
		100,
		width,
		height,
		0,
		CopyFromParent,
		CopyFromParent,
		CopyFromParent,
		CWEventMask,
		&windowAttributes
	);

	surfaceCreateInfo.sType = VK_STRUCTURE_TYPE_XLIB_SURFACE_CREATE_INFO_KHR;
	surfaceCreateInfo.dpy = dpy;
	surfaceCreateInfo.window = window;

	vkCreateXlibSurfaceKHR(instance, &surfaceCreateInfo, NULL, &surface);

	vk_open(width, height);

	XStoreName(dpy, window, name);
	XMapWindow(dpy, window);

	return;
}

/*
 * g_free - free the graphics subsystem, X11 variant
 */
void g_free(void)
{
	XUnmapWindow(dpy, window);

	vk_close();

	vkDestroySurfaceKHR(instance, surface, NULL);

	vk_free();

	XDestroyWindow(dpy, window);
	XCloseDisplay(dpy);

	return;
}

/*
 * vk_init - initialize the Vulkan instance
 */
static void vk_init(const char *name, bool validation, unsigned int platformExtensionCount, const char *platformExtensions[])
{
	int i;

	VkApplicationInfo applicationInfo = { 0 };
	VkInstanceCreateInfo instanceCreateInfo = { 0 };
	const char *enabledLayerNames[] = {
		"VK_LAYER_KHRONOS_validation"
	};
	uint32_t enabledInstanceExtensionCount = platformExtensionCount + 1;
	const char **enabledInstanceExtensionNames;

	applicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	applicationInfo.pApplicationName = name;
	applicationInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
	applicationInfo.pEngineName = NULL;
	applicationInfo.apiVersion = VK_API_VERSION_1_0;

	enabledInstanceExtensionNames = malloc(enabledInstanceExtensionCount * sizeof (*enabledInstanceExtensionNames));

	enabledInstanceExtensionNames[0] = "VK_KHR_surface";
	for (i = 1; i < enabledInstanceExtensionCount; i++)
		enabledInstanceExtensionNames[i] = platformExtensions[i - 1];

	instanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	instanceCreateInfo.pApplicationInfo = &applicationInfo;
	if (validation) {
		instanceCreateInfo.enabledLayerCount = 1;
		instanceCreateInfo.ppEnabledLayerNames = enabledLayerNames;
	} else {
		instanceCreateInfo.enabledLayerCount = 0;
	}
	instanceCreateInfo.enabledExtensionCount = enabledInstanceExtensionCount;
	instanceCreateInfo.ppEnabledExtensionNames = enabledInstanceExtensionNames;

	vkCreateInstance(&instanceCreateInfo, NULL, &instance);

	free(enabledInstanceExtensionNames);

	return;
}

/*
 * vk_open - open the connection to a Vulkan device
 */
static void vk_open(unsigned int width, unsigned int height)
{
	int i, j;

	uint32_t physicalDeviceCount;
	VkPhysicalDevice *physicalDevices;

	uint32_t physicalDeviceQueueFamilyPropertyCount;
	VkQueueFamilyProperties *physicalDeviceQueueFamilyProperties;
	int deviceQueueIndices[] = { -1, -1 };
	float *deviceQueuePriorities;
	VkDeviceQueueCreateInfo *deviceQueueCreateInfos;

	uint32_t surfaceMinImageCount;
	const char *enabledDeviceExtensionNames[] = {
		"VK_KHR_swapchain"
	};
	VkDeviceCreateInfo deviceCreateInfo = { 0 };

	const VkFormat swapchainImageFormat = VK_FORMAT_B8G8R8A8_SRGB;
	VkSwapchainCreateInfoKHR swapchainCreateInfo = { 0 };
	VkImage *swapchainImages;

	FILE *vertexShader;
	long vertexShaderModuleCodeSizeBytes;
	size_t vertexShaderModuleCodeSize;
	uint32_t *vertexShaderModuleCode;
	VkShaderModuleCreateInfo vertexShaderModuleCreateInfo = { 0 };
	FILE *fragmentShader;
	long fragmentShaderModuleCodeSizeBytes;
	size_t fragmentShaderModuleCodeSize;
	uint32_t *fragmentShaderModuleCode;
	VkShaderModuleCreateInfo fragmentShaderModuleCreateInfo = { 0 };

	VkPipelineShaderStageCreateInfo pipelineVertexShaderStageCreateInfo = { 0 };
	VkPipelineShaderStageCreateInfo pipelineFragmentShaderStageCreateInfo = { 0 };
	VkPipelineShaderStageCreateInfo pipelineShaderStageCreateInfos[2];
	VkPipelineVertexInputStateCreateInfo pipelineVertexInputStateCreateInfo = { 0 };
	VkPipelineInputAssemblyStateCreateInfo pipelineInputAssemblyStateCreateInfo = { 0 };
	VkRect2D scissor = { 0 };
	VkPipelineViewportStateCreateInfo pipelineViewportStateCreateInfo = { 0 };
	VkPipelineRasterizationStateCreateInfo pipelineRasterizationStateCreateInfo = { 0 };
	VkPipelineMultisampleStateCreateInfo pipelineMultisampleStateCreateInfo = { 0 };
	VkPipelineColorBlendAttachmentState pipelineColorBlendAttachmentState = { 0 };
	VkPipelineColorBlendStateCreateInfo pipelineColorBlendStateCreateInfo = { 0 };
	VkDynamicState dynamicStates[] = {
		VK_DYNAMIC_STATE_VIEWPORT
	};
	VkPipelineDynamicStateCreateInfo pipelineDynamicStateCreateInfo = { 0 };
	VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo = { 0 };

	VkAttachmentDescription colorAttachmentDescription = { 0 };
	VkAttachmentReference colorAttachmentReference = { 0 };

	VkSubpassDescription subpassDescription = { 0 };
	VkSubpassDependency subpassDependency = { 0 };

	VkRenderPassCreateInfo renderPassCreateInfo = { 0 };

	VkGraphicsPipelineCreateInfo graphicsPipelineCreateInfo = { 0 };

	VkCommandPoolCreateInfo commandPoolCreateInfo = { 0 };
	VkCommandBufferAllocateInfo commandBufferAllocateInfo = { 0 };

	VkSemaphoreCreateInfo semaphoreCreateInfo = { 0 };

	vkEnumeratePhysicalDevices(instance, &physicalDeviceCount, NULL);
	physicalDevices = malloc(physicalDeviceCount * sizeof (*physicalDevices));
	vkEnumeratePhysicalDevices(instance, &physicalDeviceCount, physicalDevices);
	for (i = 0; i < physicalDeviceCount; i++) {
		physicalDevice = physicalDevices[i];

		vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &physicalDeviceQueueFamilyPropertyCount, NULL);
		physicalDeviceQueueFamilyProperties = malloc(physicalDeviceQueueFamilyPropertyCount * sizeof (*physicalDeviceQueueFamilyProperties));
		vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &physicalDeviceQueueFamilyPropertyCount, physicalDeviceQueueFamilyProperties);

		for (j = 0; j < physicalDeviceQueueFamilyPropertyCount; j++) {
			VkBool32 physicalDeviceSurfaceSupport;
			VkSurfaceCapabilitiesKHR physicalDeviceSurfaceCapabilities;

			if (physicalDeviceQueueFamilyProperties[j].queueFlags & VK_QUEUE_GRAPHICS_BIT) {
				deviceQueueIndices[0] = graphicsDeviceQueueIndex = i;
			}

			vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, i, surface, &physicalDeviceSurfaceSupport);
			if (physicalDeviceSurfaceSupport) {
				vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surface, &physicalDeviceSurfaceCapabilities);

				surfaceMinImageCount = physicalDeviceSurfaceCapabilities.minImageCount;
				deviceQueueIndices[1] = surfaceDeviceQueueIndex = i;
			}
		}

		if (graphicsDeviceQueueIndex > -1 && surfaceDeviceQueueIndex > -1)
			break;

		free(physicalDeviceQueueFamilyProperties);
		deviceQueueIndices[0] = graphicsDeviceQueueIndex = -1;
		deviceQueueIndices[1] = surfaceDeviceQueueIndex = -1;
	}
	free(physicalDevices);

	deviceQueueCreateInfos = malloc(physicalDeviceQueueFamilyPropertyCount * sizeof (*deviceQueueCreateInfos));
	deviceQueuePriorities = malloc(physicalDeviceQueueFamilyPropertyCount * sizeof (*deviceQueuePriorities));
	for (i = 0; i < physicalDeviceQueueFamilyPropertyCount; i++) {
		deviceQueuePriorities[i] = 1.0f;

		deviceQueueCreateInfos[i].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		deviceQueueCreateInfos[i].pNext = NULL;
		deviceQueueCreateInfos[i].flags = 0;
		deviceQueueCreateInfos[i].queueFamilyIndex = i;
		deviceQueueCreateInfos[i].queueCount = physicalDeviceQueueFamilyProperties[i].queueCount;
		deviceQueueCreateInfos[i].pQueuePriorities = &deviceQueuePriorities[i];
	}
	free(physicalDeviceQueueFamilyProperties);

	deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	deviceCreateInfo.queueCreateInfoCount = physicalDeviceQueueFamilyPropertyCount;
	deviceCreateInfo.pQueueCreateInfos = deviceQueueCreateInfos;
	deviceCreateInfo.enabledExtensionCount = 1;
	deviceCreateInfo.ppEnabledExtensionNames = enabledDeviceExtensionNames;
	deviceCreateInfo.pEnabledFeatures = NULL;
	vkCreateDevice(physicalDevice, &deviceCreateInfo, NULL, &device);
	free(deviceQueuePriorities);
	free(deviceQueueCreateInfos);

	surfaceImageExtent.width = width;
	surfaceImageExtent.height = height;

	swapchainCreateInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	swapchainCreateInfo.surface = surface;
	swapchainCreateInfo.minImageCount = surfaceMinImageCount + 1;
	swapchainCreateInfo.imageFormat = swapchainImageFormat;
	swapchainCreateInfo.imageColorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;
	swapchainCreateInfo.imageExtent = surfaceImageExtent;
	swapchainCreateInfo.imageArrayLayers = 1;
	swapchainCreateInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	if (graphicsDeviceQueueIndex != surfaceDeviceQueueIndex) {
		swapchainCreateInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
		swapchainCreateInfo.queueFamilyIndexCount = 2;
		swapchainCreateInfo.pQueueFamilyIndices = deviceQueueIndices;
	} else {
		swapchainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
		swapchainCreateInfo.queueFamilyIndexCount = 0;
	}
	swapchainCreateInfo.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
	swapchainCreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	swapchainCreateInfo.presentMode = VK_PRESENT_MODE_FIFO_KHR;
	swapchainCreateInfo.clipped = VK_TRUE;
	swapchainCreateInfo.oldSwapchain = VK_NULL_HANDLE;
	vkCreateSwapchainKHR(device, &swapchainCreateInfo, NULL, &swapchain);

	vkGetSwapchainImagesKHR(device, swapchain, &swapchainImageCount, NULL);
	swapchainImages = malloc(swapchainImageCount * sizeof (*swapchainImages));
	vkGetSwapchainImagesKHR(device, swapchain, &swapchainImageCount, swapchainImages);
	swapchainImageViews = malloc(swapchainImageCount * sizeof (*swapchainImageViews));
	for (i = 0; i < swapchainImageCount; i++) {
		VkImageViewCreateInfo imageViewCreateInfo = { 0 };

		imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		imageViewCreateInfo.image = swapchainImages[i];
		imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		imageViewCreateInfo.format = swapchainImageFormat;
		imageViewCreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
		imageViewCreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
		imageViewCreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
		imageViewCreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
		imageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
		imageViewCreateInfo.subresourceRange.levelCount = 1;
		imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
		imageViewCreateInfo.subresourceRange.layerCount = 1;

		vkCreateImageView(device, &imageViewCreateInfo, NULL, &swapchainImageViews[i]);
	}
	free(swapchainImages);

	vertexShader = fopen("vert.spv", "rb");
	fseek(vertexShader, 0, SEEK_END);
	vertexShaderModuleCodeSizeBytes = ftell(vertexShader);
	vertexShaderModuleCodeSizeBytes += vertexShaderModuleCodeSizeBytes % sizeof (*vertexShaderModuleCode);
	vertexShaderModuleCodeSize = vertexShaderModuleCodeSizeBytes / sizeof (*vertexShaderModuleCode);
	vertexShaderModuleCode = malloc(vertexShaderModuleCodeSize * sizeof (*vertexShaderModuleCode));
	fseek(vertexShader, 0, SEEK_SET);
	fread(vertexShaderModuleCode, sizeof (*vertexShaderModuleCode), vertexShaderModuleCodeSize, vertexShader);
	fclose(vertexShader);
	vertexShaderModuleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	vertexShaderModuleCreateInfo.codeSize = vertexShaderModuleCodeSizeBytes;
	vertexShaderModuleCreateInfo.pCode = vertexShaderModuleCode;
	vkCreateShaderModule(device, &vertexShaderModuleCreateInfo, NULL, &vertexShaderModule);
	free(vertexShaderModuleCode);

	fragmentShader = fopen("frag.spv", "rb");
	fseek(fragmentShader, 0, SEEK_END);
	fragmentShaderModuleCodeSizeBytes = ftell(fragmentShader);
	fragmentShaderModuleCodeSizeBytes += fragmentShaderModuleCodeSizeBytes % sizeof (*fragmentShaderModuleCode);
	fragmentShaderModuleCodeSize = fragmentShaderModuleCodeSizeBytes / sizeof (*fragmentShaderModuleCode);
	fragmentShaderModuleCode = malloc(fragmentShaderModuleCodeSize * sizeof (*fragmentShaderModuleCode));
	fseek(fragmentShader, 0, SEEK_SET);
	fread(fragmentShaderModuleCode, sizeof (*fragmentShaderModuleCode), fragmentShaderModuleCodeSize, fragmentShader);
	fclose(fragmentShader);
	fragmentShaderModuleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	fragmentShaderModuleCreateInfo.codeSize = fragmentShaderModuleCodeSizeBytes;
	fragmentShaderModuleCreateInfo.pCode = fragmentShaderModuleCode;
	vkCreateShaderModule(device, &fragmentShaderModuleCreateInfo, NULL, &fragmentShaderModule);
	free(fragmentShaderModuleCode);

	pipelineVertexShaderStageCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	pipelineVertexShaderStageCreateInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
	pipelineVertexShaderStageCreateInfo.module = vertexShaderModule;
	pipelineVertexShaderStageCreateInfo.pName = "main";
	pipelineVertexShaderStageCreateInfo.pSpecializationInfo = NULL;
	pipelineShaderStageCreateInfos[0] = pipelineVertexShaderStageCreateInfo;

	pipelineFragmentShaderStageCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	pipelineFragmentShaderStageCreateInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
	pipelineFragmentShaderStageCreateInfo.module = fragmentShaderModule;
	pipelineFragmentShaderStageCreateInfo.pName = "main";
	pipelineFragmentShaderStageCreateInfo.pSpecializationInfo = NULL;
	pipelineShaderStageCreateInfos[1] = pipelineFragmentShaderStageCreateInfo;

	pipelineVertexInputStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	pipelineVertexInputStateCreateInfo.vertexBindingDescriptionCount = 0;
	pipelineVertexInputStateCreateInfo.vertexAttributeDescriptionCount = 0;

	pipelineInputAssemblyStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	pipelineInputAssemblyStateCreateInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	pipelineInputAssemblyStateCreateInfo.primitiveRestartEnable = VK_FALSE;

	viewport.x = 0.0f;
	viewport.y = 0.0f;
	viewport.width = (float) surfaceImageExtent.width;
	viewport.height = (float) surfaceImageExtent.height;
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;

	scissor.offset.x = 0;
	scissor.offset.y = 0;
	scissor.extent = surfaceImageExtent;

	pipelineViewportStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	pipelineViewportStateCreateInfo.viewportCount = 1;
	pipelineViewportStateCreateInfo.pViewports = &viewport;
	pipelineViewportStateCreateInfo.scissorCount = 1;
	pipelineViewportStateCreateInfo.pScissors = &scissor;

	pipelineRasterizationStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	pipelineRasterizationStateCreateInfo.depthClampEnable = VK_FALSE;
	pipelineRasterizationStateCreateInfo.rasterizerDiscardEnable = VK_FALSE;
	pipelineRasterizationStateCreateInfo.polygonMode = VK_POLYGON_MODE_FILL;
	pipelineRasterizationStateCreateInfo.cullMode = VK_CULL_MODE_BACK_BIT;
	pipelineRasterizationStateCreateInfo.frontFace = VK_FRONT_FACE_CLOCKWISE;
	pipelineRasterizationStateCreateInfo.depthBiasEnable = VK_FALSE;
	pipelineRasterizationStateCreateInfo.lineWidth = 1.0f;

	pipelineMultisampleStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	pipelineMultisampleStateCreateInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
	pipelineMultisampleStateCreateInfo.sampleShadingEnable = VK_FALSE;

	pipelineColorBlendAttachmentState.blendEnable = VK_FALSE;
	pipelineColorBlendAttachmentState.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

	pipelineColorBlendStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	pipelineColorBlendStateCreateInfo.logicOpEnable = VK_FALSE;
	pipelineColorBlendStateCreateInfo.attachmentCount = 1;
	pipelineColorBlendStateCreateInfo.pAttachments = &pipelineColorBlendAttachmentState;

	pipelineDynamicStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
	pipelineDynamicStateCreateInfo.dynamicStateCount = 1;
	pipelineDynamicStateCreateInfo.pDynamicStates = dynamicStates;

	pipelineLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	vkCreatePipelineLayout(device, &pipelineLayoutCreateInfo, NULL, &pipelineLayout);

	colorAttachmentDescription.format = swapchainImageFormat;
	colorAttachmentDescription.samples = VK_SAMPLE_COUNT_1_BIT;
	colorAttachmentDescription.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	colorAttachmentDescription.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	colorAttachmentDescription.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	colorAttachmentDescription.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	colorAttachmentDescription.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	colorAttachmentDescription.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

	colorAttachmentReference.attachment = 0;
	colorAttachmentReference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	subpassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	subpassDescription.colorAttachmentCount = 1;
	subpassDescription.pColorAttachments = &colorAttachmentReference;

	subpassDependency.srcSubpass = VK_SUBPASS_EXTERNAL;
	subpassDependency.dstSubpass = 0;
	subpassDependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	subpassDependency.srcAccessMask = 0;
	subpassDependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	subpassDependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

	renderPassCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	renderPassCreateInfo.attachmentCount = 1;
	renderPassCreateInfo.pAttachments = &colorAttachmentDescription;
	renderPassCreateInfo.subpassCount = 1;
	renderPassCreateInfo.pSubpasses = &subpassDescription;
	renderPassCreateInfo.dependencyCount = 1;
	renderPassCreateInfo.pDependencies = &subpassDependency;
	vkCreateRenderPass(device, &renderPassCreateInfo, NULL, &renderPass);

	graphicsPipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	graphicsPipelineCreateInfo.stageCount = 2;
	graphicsPipelineCreateInfo.pStages = pipelineShaderStageCreateInfos;
	graphicsPipelineCreateInfo.pVertexInputState = &pipelineVertexInputStateCreateInfo;
	graphicsPipelineCreateInfo.pInputAssemblyState = &pipelineInputAssemblyStateCreateInfo;
	graphicsPipelineCreateInfo.pViewportState = &pipelineViewportStateCreateInfo;
	graphicsPipelineCreateInfo.pRasterizationState = &pipelineRasterizationStateCreateInfo;
	graphicsPipelineCreateInfo.pMultisampleState = &pipelineMultisampleStateCreateInfo;
	graphicsPipelineCreateInfo.pColorBlendState = &pipelineColorBlendStateCreateInfo;
	graphicsPipelineCreateInfo.pDynamicState = &pipelineDynamicStateCreateInfo;
	graphicsPipelineCreateInfo.layout = pipelineLayout;
	graphicsPipelineCreateInfo.renderPass = renderPass;
	graphicsPipelineCreateInfo.subpass = 0;
	vkCreateGraphicsPipelines(device, VK_NULL_HANDLE, 1, &graphicsPipelineCreateInfo, NULL, &graphicsPipeline);

	swapchainFramebuffers = malloc(swapchainImageCount * sizeof (*swapchainFramebuffers));
	for (i = 0; i < swapchainImageCount; i++) {
		VkFramebufferCreateInfo framebufferCreateInfo = { 0 };

		framebufferCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		framebufferCreateInfo.renderPass = renderPass;
		framebufferCreateInfo.attachmentCount = 1;
		framebufferCreateInfo.pAttachments = &swapchainImageViews[i];
		framebufferCreateInfo.width = surfaceImageExtent.width;
		framebufferCreateInfo.height = surfaceImageExtent.height;
		framebufferCreateInfo.layers = 1;

		vkCreateFramebuffer(device, &framebufferCreateInfo, NULL, &swapchainFramebuffers[i]);
	}

	commandPoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	commandPoolCreateInfo.queueFamilyIndex = graphicsDeviceQueueIndex;
	commandPoolCreateInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
	vkCreateCommandPool(device, &commandPoolCreateInfo, NULL, &commandPool);

	commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	commandBufferAllocateInfo.commandPool = commandPool;
	commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	commandBufferAllocateInfo.commandBufferCount = swapchainImageCount;
	commandBuffers = malloc(swapchainImageCount * sizeof (*commandBuffers));
	vkAllocateCommandBuffers(device, &commandBufferAllocateInfo, commandBuffers);

	semaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
	vkCreateSemaphore(device, &semaphoreCreateInfo, NULL, &imageAvailableSemaphore);
	vkCreateSemaphore(device, &semaphoreCreateInfo, NULL, &renderFinishedSemaphore);

	return;
}

/*
 * vk_close - close the connection to a Vulkan device
 */
static void vk_close(void)
{
	int i;

	vkDestroySemaphore(device, renderFinishedSemaphore, NULL);
	vkDestroySemaphore(device, imageAvailableSemaphore, NULL);

	free(commandBuffers);
	vkDestroyCommandPool(device, commandPool, NULL);

	for (i = swapchainImageCount - 1; i >= 0; --i)
		vkDestroyFramebuffer(device, swapchainFramebuffers[i], NULL);
	free(swapchainFramebuffers);

	vkDestroyPipeline(device, graphicsPipeline, NULL);
	vkDestroyRenderPass(device, renderPass, NULL);
	vkDestroyPipelineLayout(device, pipelineLayout, NULL);
	vkDestroyShaderModule(device, fragmentShaderModule, NULL); 
	vkDestroyShaderModule(device, vertexShaderModule, NULL);

	for (i = swapchainImageCount - 1; i >= 0; --i)
		vkDestroyImageView(device, swapchainImageViews[i], NULL);
	free(swapchainImageViews);

	vkDestroySwapchainKHR(device, swapchain, NULL);
	vkDestroyDevice(device, NULL);

	return;
}

/*
 * vk_free - free the Vulkan instance
 */
static void vk_free(void)
{
	vkDestroyInstance(instance, NULL);

	return;
}
