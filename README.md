# vulkan-scratch

## Rationale
This is a simple scratchpad project for Vulkan. I'll be using this repo to track miscellaneous Vulkan work as I'm learning the API. This repo caters to no single iteration, and if any distinct projects come out of my tinkering, these will be stood up as their own individual projects.

## Licensing
I'm not going to attach any sort of formal license to this repo. Consider anything that pops up here public domain.

## Liability
Given that I'm not licensing this software, but providing it publicly without license, this software is distributed AS-IS and is not fit for any distinct purpose. I'm not responsible for what you do with this code, so don't come crying to me if your computer bursts into flames. This is just for my own use, any resemblance to a working program or library is purely coincidental, and I make no guarantees that the contents herein will be conformant to any expectation.

## Environment
Currently I'm developing on GNU/Linux, specifically on a Raspberry Pi 400, using Mesa3D on top of libdrm through Xlib. Additionally, I am using the various Vulkan development components distributed by Khronos and suggested by LunarG. The intent is for this codebase to be compatible with any X11 environment supporting Vulkan. I also intend to eventually abstract out the WSI pieces to allow for Win32 and Cocoa targets as well. If all goes according to plan, this application should at a minimum depend on the ANSI C library, Vulkan, and the native windowing system of the underlying OS. Anywhere that system-level programming is needed that cannot be satisfied by the ANSI C library, Win32 will be used on Windows and POSIX constructs on macOS, GNU/Linux, BSD, etc. Threading will be an eventual goal but will not be considered from the start, so code may not be thread safe until such implementation.

## Building
Currently, to build on a Unix system with X11, use make. You should get the resulting binary in the parent folder of the package if all goes well. This Makefile operates on the assumption that you are using a POSIX-compatible make with SUFFIX definitions for .c.o. If this is not the case, you will need to implement a "%.o: %.c" or ".c.o" rule yourself.
