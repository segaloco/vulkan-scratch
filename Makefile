#
# vulkan-scratch - Simple vulkan template
#

CC			=	c99	
CFLAGS		=	-O2 -g
LDFLAGS		=	-lX11 -lvulkan

GLSLC		=	glslangValidator
GLSLCFLAGS	=	-V

RM			=	rm -rf

BIN			=	vulkan-scratch

MAIN		=	src/main.o

GRAPHICS	=	src/graphics/g_core.o

INPUT		=	src/input/i_core.o

OBJS		=	$(MAIN) \
				$(GRAPHICS) \
				$(INPUT)

SPIRV_OBJS	=	src/graphics/shaders/vertex.spv \
				src/graphics/shaders/fragment.spv

$(BIN): $(OBJS) $(SPIRV_OBJS)
	$(CC) -o $(BIN) $(OBJS) $(LDFLAGS)

%.spv: %.vert
	$(GLSLC) $(GLSLCFLAGS) $<

%.spv: %.frag
	$(GLSLC) $(GLSLCFLAGS) $<

clean:
	$(RM) $(BIN) $(OBJS) *.spv
